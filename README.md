## proxy-grabber

# TODO
* load proxy from db
* usable for (google yandex etc.)
* add more sites
* add htmlunit support
* check coutnry curl 'http://gd.geobytes.com/GetIpServiceArea?ip=77.75.152.6'


## To build

    ./gradlew clean build

### To do a complete package deb build

    ./gradlew clean build buildDeb

## To run

    ./gradlew clean bootRun

Once the application is running, navigate to `http://localhost:8080/` in your browser (or use `curl` or any HTTP client)
and you should see the text "Hello world!".


#Proxy grabber 
## Описание
Приложение хранит в себе proxy полученные из разных источников, служит провайдером proxy разных типов из разных стран

##Требования

* Валидировать proxy на придмет доступности 
* Определять тип proxy
* Определять страну proxy
* Хранить перечень всех proxy 
* Хранить перечень живых proxy и уметь быстро чекать/отдавать их по требованию
* Парсить proxy из разных источников freeproxy-list.ru, hideme.ru etc. 
* Добавлять proxy POST http://host:port/proxy-grabber/add-proxy
* Поулчать proxy GET http://host:port/proxy-grabber/get-proxy?country=?&type=?
* Получать статистику proxy-grabber http://host:port/proxy-grabber/statistic

## Реализация
Был выбран kotlin, spring 5.0 springRx
https://ip-ranges.amazonaws.com/ip-ranges.json
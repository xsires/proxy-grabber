group 'grabber-grabber-kotlin'
version '0.1'

buildscript {
    ext.kotlin_version = '1.1.1'
    ext.xodus_version = '1.0.4'
    ext.jsoup_version = '1.10.2'
    ext.html_unit_version = '2.26'
    ext.sprint_boot_version = '2.0.0.BUILD-SNAPSHOT'

    repositories {
        mavenCentral()
    }
    dependencies {
        classpath "org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlin_version"
    }
}

apply plugin: 'kotlin'


repositories {
    mavenCentral()
    maven {
        url 'https://repo.spring.io/libs-snapshot'
    }
}

dependencies {
    compile "org.jetbrains.kotlin:kotlin-stdlib:$kotlin_version"
    compile("com.neovisionaries:nv-i18n:1.21")

    compile("org.springframework.boot:spring-boot-starter-web:$sprint_boot_version")

    compile "org.jetbrains.xodus:xodus-openAPI:$xodus_version"
    compile "org.jetbrains.xodus:xodus-environment:$xodus_version"
    compile "org.jetbrains.xodus:xodus-entity-store:$xodus_version"

    // https://mvnrepository.com/artifact/org.jsoup/jsoup
    compile group: 'org.jsoup', name: 'jsoup', version: '1.10.2'

    compile "net.sourceforge.htmlunit:htmlunit:$html_unit_version"

    testCompile("org.springframework.boot:spring-boot-starter-test:$sprint_boot_version")
    testCompile("junit:junit:4.12")
    testCompile("org.unitils:unitils-core:3.4.2")
}

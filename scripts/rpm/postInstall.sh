#!/bin/sh
chown proxy-grabber:proxy-grabber /usr/local/proxy-grabber/log


PHANTOMJS=phantomjs-2.1.1-linux-x86_64.tar.bz2
rm -rf /usr/local/proxy-grabber/phantomjs
tar xvjf /usr/local/proxy-grabber/conf/$PHANTOMJS -C /usr/local/proxy-grabber
mv /usr/local/proxy-grabber/phantomjs* /usr/local/proxy-grabber/phantomjs
rm /usr/local/proxy-grabber/conf/$PHANTOMJS
chown -R proxy-grabber:proxy-grabber /usr/local/proxy-grabber/phantomjs


sudo service proxy-grabber restart

#// we cant install another package while install current
#sudo apt-get install libfontconfig
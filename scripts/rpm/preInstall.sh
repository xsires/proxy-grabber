#!/bin/sh
echo "Creating group: proxy-grabber"
/usr/sbin/groupadd -f -r proxy-grabber 2> /dev/null || :

echo "Creating user: proxy-grabber"
/usr/sbin/useradd -r -m -c "proxy-grabber user" proxy-grabber -g proxy-grabber 2> /dev/null || :

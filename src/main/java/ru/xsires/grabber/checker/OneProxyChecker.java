package ru.xsires.grabber.checker;

import ru.xsires.grabber.proxy.Proxy;
import ru.xsires.grabber.proxy.ProxyValidationInfo;

public interface OneProxyChecker {

    boolean checkAndIncrementFailAttemptCount(ProxyValidationInfo proxyValidationInfo);

    boolean check(Proxy validationInformationProxy);

    Proxy.Type checkAndGetType(Proxy validationInformationProxy);
}

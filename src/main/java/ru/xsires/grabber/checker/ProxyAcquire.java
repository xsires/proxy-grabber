package ru.xsires.grabber.checker;

import com.neovisionaries.i18n.CountryCode;
import ru.xsires.grabber.proxy.Proxy;

import javax.validation.constraints.NotNull;

public interface ProxyAcquire {

    Proxy get(Proxy.Type type, CountryCode country);

}

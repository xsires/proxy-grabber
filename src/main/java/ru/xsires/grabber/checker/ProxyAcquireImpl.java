package ru.xsires.grabber.checker;

import com.neovisionaries.i18n.CountryCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.xsires.grabber.container.ProxyContainer;
import ru.xsires.grabber.proxy.Proxy;
import ru.xsires.grabber.proxy.ProxyValidationInfo;

import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.concurrent.*;

public class ProxyAcquireImpl implements ProxyAcquire {
    private static final Logger log = LoggerFactory.getLogger(ProxyAcquireImpl.class);


    private final ProxyContainer proxyContainer;
    private final ExecutorService executorService;
    private final long maxAwaitMillis;
    private final int checkBatchSize;
    private final OneProxyChecker oneProxyChecker;

    public ProxyAcquireImpl(ProxyContainer proxyContainer, ExecutorService executorService, OneProxyChecker oneProxyChecker,
                            long maxAwaitMillis,
                            int checkBatchSize) {
        this.proxyContainer = proxyContainer;
        this.executorService = executorService;
        this.oneProxyChecker = oneProxyChecker;
        this.maxAwaitMillis = maxAwaitMillis;
        this.checkBatchSize = checkBatchSize;
    }

    public static <T extends Object> List<T[]> splitArray(T[] array, int max) {
        int x = array.length / max;
        int r = (array.length % max); // remainder
        int lower = 0;
        int upper = 0;
        List<T[]> list = new ArrayList<>();
        for (int i = 0; i < x; i++) {
            upper += max;
            list.add(Arrays.copyOfRange(array, lower, upper));
            lower = upper;
        }
        if (r > 0) {
            list.add(Arrays.copyOfRange(array, lower, (lower + r)));
        }
        return list;
    }

    @Override
    public Proxy get(Proxy.Type type, CountryCode country) {

        List<ProxyValidationInfo> proxyList = proxyContainer.get(type, country);
        if (proxyList.size() == 0) {
            return null;
        } else if (proxyList.size() == 1) {
            if (oneProxyChecker.checkAndIncrementFailAttemptCount(proxyList.get(0))) {
                return proxyList.get(0).getProxy();
            } else {
                return null;
            }
        }

        Collections.shuffle(proxyList);
        List<ProxyValidationInfo[]> validationInformationProxies = splitArray(proxyList.toArray(new ProxyValidationInfo[proxyList.size()]), checkBatchSize);
        for (ProxyValidationInfo[] proxyValidationInfo : validationInformationProxies) {
            Proxy proxy = acquirePack(proxyValidationInfo);
            if (proxy != null) {
                return proxy;
            }

        }


        return null;
    }

    private Proxy acquirePack(final ProxyValidationInfo[] proxyCollections) {

        final CountDownLatch checkThreadCountDownLatch = new CountDownLatch(proxyCollections.length);
        BlockingQueue<ProxyHolder> resultQueue = new LinkedBlockingQueue<>();
        executorService.submit(new NothingIsFound(checkThreadCountDownLatch, resultQueue));

        Arrays.stream(proxyCollections).forEach(p -> executorService.submit(new AsyncCheck(p, checkThreadCountDownLatch, resultQueue)));
        try {
            return resultQueue.poll(maxAwaitMillis, TimeUnit.MILLISECONDS).getProxy();
        } catch (InterruptedException e) {
            log.error("InterruptedException ", e);
        }
        return null;
    }

    private class ProxyHolder {
        private final Proxy proxy;

        public ProxyHolder(Proxy proxy) {
            this.proxy = proxy;
        }

        public Proxy getProxy() {
            return proxy;
        }
    }

    private class NothingIsFound implements Runnable {
        private final CountDownLatch countDownLatch;
        private final Queue<ProxyHolder> resultQueue;

        private NothingIsFound(CountDownLatch countDownLatch, BlockingQueue<ProxyHolder> resultQueue) {
            this.countDownLatch = countDownLatch;
            this.resultQueue = resultQueue;
        }

        @Override
        public void run() {
            try {
                countDownLatch.await();
            } catch (InterruptedException e) {
                log.error("NothingIsFound ", e);
            } finally {
                if (resultQueue.peek() == null) {
                    resultQueue.add(new ProxyHolder(null));
                }

            }

        }
    }

    private class AsyncCheck implements Runnable {
        private final ProxyValidationInfo informationProxy;
        private final CountDownLatch countDownLatch;
        private final Queue<ProxyHolder> resultQueue;


        private AsyncCheck(ProxyValidationInfo informationProxy, CountDownLatch countDownLatch, Queue<ProxyHolder> resultQueue) {
            this.informationProxy = informationProxy;
            this.countDownLatch = countDownLatch;
            this.resultQueue = resultQueue;
        }

        @Override
        public void run() {
            try {
                if (oneProxyChecker.checkAndIncrementFailAttemptCount(informationProxy)) {
                    resultQueue.add(new ProxyHolder(informationProxy.getProxy()));
                }
            } finally {
                countDownLatch.countDown();
            }
        }
    }

}

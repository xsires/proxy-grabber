package ru.xsires.grabber.config;

public class CheckerConfig {

    private int checkProxyConnectionTimeOut;
    private long maxAwaitMillis;
    private int checkBatchSize;

    public long getMaxAwaitMillis() {
        return maxAwaitMillis;
    }

    public void setMaxAwaitMillis(long maxAwaitMillis) {
        this.maxAwaitMillis = maxAwaitMillis;
    }

    public int getCheckProxyConnectionTimeOut() {
        return checkProxyConnectionTimeOut;
    }

    public void setCheckProxyConnectionTimeOut(int checkProxyConnectionTimeOut) {
        this.checkProxyConnectionTimeOut = checkProxyConnectionTimeOut;
    }

    public int getCheckBatchSize() {
        return checkBatchSize;
    }

    public void setCheckBatchSize(int checkBatchSize) {
        this.checkBatchSize = checkBatchSize;
    }

    @Override
    public String toString() {
        return "CheckerConfig{" +
                "checkProxyConnectionTimeOut=" + checkProxyConnectionTimeOut +
                ", maxAwaitMillis=" + maxAwaitMillis +
                ", checkBatchSize=" + checkBatchSize +
                '}';
    }
}

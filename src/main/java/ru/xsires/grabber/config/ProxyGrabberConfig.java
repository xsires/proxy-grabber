package ru.xsires.grabber.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

//@ConfigurationProperties(prefix = "proxy-grabber")
@PropertySources({
        @PropertySource("file:${config.directory}/proxy-grabber.properties"),
        @PropertySource("classpath:${config.override}")}
)
//@Configuration
public class ProxyGrabberConfig {

    private CheckerConfig checker;
    private int threadpoolCoreSize;

    public int getThreadpoolCoreSize() {
        return threadpoolCoreSize;
    }

    public void setThreadpoolCoreSize(int threadpoolCoreSize) {
        this.threadpoolCoreSize = threadpoolCoreSize;
    }

    public CheckerConfig getChecker() {
        return checker;
    }

    public void setChecker(CheckerConfig checker) {
        this.checker = checker;
    }


    @Override
    public String toString() {
        return "ProxyGrabberConfig{" +
                ", checker=" + checker +
                ", threadpoolCoreSize=" + threadpoolCoreSize +
                '}';
    }
}

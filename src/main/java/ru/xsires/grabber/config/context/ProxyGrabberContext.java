package ru.xsires.grabber.config.context;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import ru.xsires.grabber.checker.*;
import ru.xsires.grabber.config.ProxyGrabberConfig;
import ru.xsires.grabber.container.ProxyContainer;
import ru.xsires.grabber.container.ProxyContainerImpl;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;

//@Configuration
@Import(value = {
        ProxyGrabberConfig.class})
public class ProxyGrabberContext {

    @Bean
    public ScheduledExecutorService createExecutor(ProxyGrabberConfig config) {
        return new ScheduledThreadPoolExecutor(config.getThreadpoolCoreSize());
    }


    @Bean
    public ProxyAcquireImpl proxyAcquires(ExecutorService executorService, OneProxyChecker oneProxyChecker, ProxyContainer proxyContainer, ProxyGrabberConfig config) {
        return new ProxyAcquireImpl(proxyContainer,
                executorService,
                oneProxyChecker,
                config.getChecker().getMaxAwaitMillis(),
                config.getChecker().getCheckBatchSize());
    }

}
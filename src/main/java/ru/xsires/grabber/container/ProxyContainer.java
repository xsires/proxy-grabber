package ru.xsires.grabber.container;

import com.neovisionaries.i18n.CountryCode;
import ru.xsires.grabber.proxy.Proxy;
import ru.xsires.grabber.proxy.ProxyValidationInfo;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

public interface ProxyContainer {

    ProxyValidationInfo put(@NotNull Proxy proxy);
    List<ProxyValidationInfo> getByTypesAndCountries(Set<Proxy.Type> type, Set<CountryCode> country);
    List<ProxyValidationInfo> get(Proxy.Type type, CountryCode country);
    List<ProxyValidationInfo> getAll();
}

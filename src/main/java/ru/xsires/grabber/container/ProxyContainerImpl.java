package ru.xsires.grabber.container;

import com.google.common.collect.ImmutableSet;
import com.neovisionaries.i18n.CountryCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.xsires.grabber.proxy.Proxy;
import ru.xsires.grabber.proxy.ProxyValidationInfo;

import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class ProxyContainerImpl implements ProxyContainer, Runnable {

    private static final Logger log = LoggerFactory.getLogger(ProxyContainerImpl.class);

    private final int failAttemptCountToAbortProxy;
    private final Object DUMMY = new Object();

    private final Map<ProxyValidationInfo, Object> container = new ConcurrentHashMap<>();

    public ProxyContainerImpl(int failAttemptCountToAbortProxy) {
        this.failAttemptCountToAbortProxy = failAttemptCountToAbortProxy;
    }

    @Override
    public ProxyValidationInfo put(final @NotNull Proxy proxy) {
        log.info("Add proxy: " + proxy);
        ProxyValidationInfo key = new ProxyValidationInfo(proxy);
        Object putIfAbsent = container.putIfAbsent(key, DUMMY);
        if (putIfAbsent == null) {
            return key;
        } else {
            return null;
        }

    }


    @Override
    public void run() {
        removeBrokenProxies();
    }

    private void removeBrokenProxies() {
        container.keySet().removeIf(p -> {
            if (p.getFailCheckAttemptCount() > failAttemptCountToAbortProxy) {
                log.info("remove " + p.getProxy());
                return true;
            } else {
                return false;
            }
        });
    }

    @Override
    public List<ProxyValidationInfo> get(Proxy.Type type, CountryCode country) {
        return getByTypesAndCountries(type != null ? ImmutableSet.of(type) : Collections.emptySet(),
                country != null ? ImmutableSet.of(country) : Collections.emptySet());
    }

    @Override
    public List<ProxyValidationInfo> getAll() {
        return get(null, null);
    }




    @Override
    public List<ProxyValidationInfo> getByTypesAndCountries(Set<Proxy.Type> type, Set<CountryCode> country) {
        Objects.requireNonNull(country);
        Objects.requireNonNull(type);
        if (type.isEmpty() && country.isEmpty() || country.equals(ImmutableSet.of(CountryCode.UNDEFINED))) {
            return new ArrayList<>(container.keySet());
        }
        return container.keySet().stream()
                .filter(p -> type.contains(p.getProxy().getType()))
                .filter(p -> country == CountryCode.UNDEFINED || Objects.equals(p.getProxy().getCountry(), country)).collect(Collectors.toList());
    }
}

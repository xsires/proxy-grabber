package ru.xsires.grabber.proxy;

import com.neovisionaries.i18n.CountryCode;

import java.net.InetSocketAddress;
import java.util.Objects;

public class Proxy {

    private InetSocketAddress address;
    private CountryCode country;
    private Type type;

    public Proxy(String host, int port, CountryCode countryCode, Type type) {
        this(new InetSocketAddress(host, port), countryCode, type);
    }

    public Proxy(InetSocketAddress address, CountryCode country, Type type) {
        this.address = Objects.requireNonNull(address);
        this.country = Objects.requireNonNull(country);
        this.type = Objects.requireNonNull(type);
    }

    public InetSocketAddress getAddress() {
        return address;
    }

    public String getHostName() {
        return getAddress().getHostName();
    }

    public int getPort() {
        return getAddress().getPort();
    }

    public CountryCode getCountry() {
        return country;
    }

    public Type getType() {
        return type;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Proxy proxy = (Proxy) o;
        return address != null ? address.equals(proxy.address) : proxy.address == null;
    }


    @Override
    public int hashCode() {
        return address != null ? address.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Proxy{" +
                "address=" + address +
                ", country='" + country + '\'' +
                ", type=" + type +
                '}';
    }

    public enum Type {
        UNDEFINED, HTTP, HTTPS, SOCKS;


        public java.net.Proxy.Type getNetType() {
            if (this == Type.HTTP || this == Type.HTTPS) {
                return java.net.Proxy.Type.HTTP;
            } else if (this == SOCKS) {
                return java.net.Proxy.Type.SOCKS;
            } else if (this == UNDEFINED) {
                // fixme
                return java.net.Proxy.Type.SOCKS;
            } else {
                throw new UnsupportedOperationException("unimplemented");
            }

        }
    }
}

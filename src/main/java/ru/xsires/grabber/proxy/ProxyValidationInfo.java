package ru.xsires.grabber.proxy;

import java.util.Date;
import java.util.concurrent.atomic.AtomicLong;

public class ProxyValidationInfo {
    private final Proxy proxy;
    private final Date create;
    private final AtomicLong failCheckAttemptCount = new AtomicLong();
    private volatile Date lastAvailableDate;


    public ProxyValidationInfo(Proxy proxy) {
        this.proxy = proxy;
        this.create = new Date();
    }

    public ProxyValidationInfo(Proxy proxy, Date create, Date lastAvailableDate) {
        this.proxy = proxy;
        this.create = create;
        this.lastAvailableDate = lastAvailableDate;
    }

    public void incrementFailCheckAttemptCount() {
        failCheckAttemptCount.incrementAndGet();
    }

    public long getFailCheckAttemptCount() {
        return failCheckAttemptCount.get();
    }

    public Proxy getProxy() {
        return proxy;
    }

    public Date getCreate() {
        return create;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProxyValidationInfo that = (ProxyValidationInfo) o;

        return proxy != null ? proxy.equals(that.proxy) : that.proxy == null;

    }

    public void setLastAvailableDateAsCurrent() {
        lastAvailableDate = new Date();
    }

    public Date getLastAvailableDate() {
        return lastAvailableDate;
    }

    @Override
    public int hashCode() {
        return proxy != null ? proxy.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "ProxyValidationInfo{" +
                "proxy=" + proxy +
                ", failCheckAttemptCount=" + failCheckAttemptCount +
                '}';
    }
}
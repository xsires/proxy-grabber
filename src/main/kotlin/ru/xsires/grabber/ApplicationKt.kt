package ru.xsires.grabber

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
@ComponentScan("ru.xsires")
open class ApplicationKt {
    companion object {
        @JvmStatic fun main(args: Array<String>) {
            SpringApplication.run(ApplicationKt::class.java, *args)
        }
    }
}
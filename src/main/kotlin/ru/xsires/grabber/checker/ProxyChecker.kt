package ru.xsires.grabber.checker

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.xsires.grabber.config.ProxyCheckerConfig
import ru.xsires.grabber.model.Address
import ru.xsires.grabber.model.ProxyType
import java.net.InetSocketAddress
import java.net.Socket


/**
 * Проверят доступности прокси
 */
@Component
open class ProxyChecker(@param:Autowired val checkerConfig: ProxyCheckerConfig) {

    companion object {
        val log = LoggerFactory.getLogger(ProxyChecker::class.java)
    }

    fun check(proxy: Address): Boolean {
        log.info("Check: {}", proxy.toString())
        try {
            Socket().use {
                it.connect(InetSocketAddress(proxy.address, proxy.port), checkerConfig.checkProxyConnectionTimeOut)
                return true
            }
        } catch (e: Exception) {
            return false
        }
    }
}


interface ProxyTypeDefiner {
    fun defineType(proxy: Address): ProxyType
}
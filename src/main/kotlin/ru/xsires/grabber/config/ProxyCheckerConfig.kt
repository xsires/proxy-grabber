package ru.xsires.grabber.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource


@Configuration
@PropertySource("file:\${config.directory}/proxy-grabber.properties")
@ConfigurationProperties(prefix = "proxy-grabber.checker")
open class ProxyCheckerConfig {
    var maxAwaitMillis: Int? = null
    var checkProxyConnectionTimeOut: Int = 10000
    var checkBatchSize: Int? = null
}
package ru.xsires.grabber.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.ScheduledThreadPoolExecutor


@Configuration
open class ProxyGrabberContext() {

    @Bean
    open fun createExecutor(): ScheduledExecutorService {
        return ScheduledThreadPoolExecutor(150)
    }
}

package ru.xsires.grabber.db

import jetbrains.exodus.ByteIterable
import jetbrains.exodus.ByteIterableBase
import jetbrains.exodus.bindings.StringBinding
import jetbrains.exodus.env.*
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.xsires.grabber.config.ProxyStorageConfig
import ru.xsires.grabber.model.Address
import ru.xsires.grabber.model.ProxyStorageModel
import ru.xsires.grabber.monitoring.DBStatistic
import java.util.*
import java.util.Optional.ofNullable
import java.util.function.Consumer
import javax.annotation.PostConstruct

@Component
open class ProxyDao(@Autowired val storageConfig: ProxyStorageConfig,
                    @Autowired val serializer: ProxySerializer) {
    companion object {
        val log = LoggerFactory.getLogger(ProxyDao::class.java)
    }

    lateinit var environment: Environment

    @PostConstruct
    fun init() {
        environment = Environments.newInstance(storageConfig.location)
    }

    fun statistic(): DBStatistic {
        var count = 0
        var lastAvailable = 0
        iterateAll(Consumer {
            count++
            if (it.lastAvailable != null) {
                lastAvailable++
            }
        })
        return DBStatistic(count, lastAvailable)

    }

    private fun openStore(transaction: Transaction): Store {
        return environment.openStore("proxy", StoreConfig.WITHOUT_DUPLICATES, transaction)
    }


    fun save(proxyStorageModels: Collection<ProxyStorageModel>) {
        log.info("save(): proxyStorageModels={}", proxyStorageModels)
        environment.executeInTransaction {
            val openStore = openStore(it)
            for (proxyStorageModel in proxyStorageModels) {
                openStore.put(it, getId(proxyStorageModel), StringBinding.stringToEntry(serializer.serialize(proxyStorageModel)))
            }
        }
    }

    fun save(proxyStorageModel: ProxyStorageModel) {
        log.info("save(): proxyStorageModels={}", proxyStorageModel)
        environment.executeInTransaction {
            openStore(it)
                    .put(it, getId(proxyStorageModel), StringBinding.stringToEntry(serializer.serialize(proxyStorageModel)))
        }
    }

    fun merge(proxyStorageModel: ProxyStorageModel) {
        save(proxyStorageModel.merge(loadByAddress(proxyStorageModel.address)))
    }

    fun loadByAddress(address: Address): ProxyStorageModel? {
        return environment.computeInReadonlyTransaction {
            ofNullable(openStore(it)
                    .get(it, getId(address)))
                    .map { deserialize(it) }
                    .orElseGet { null }
        }
    }

    fun clean() {
        environment.executeInTransaction {
            val store = openStore(it)
            val openCursor = store.openCursor(it)
            while (openCursor.next) {
                store.delete(it, openCursor.key)
            }
        }
    }

    fun loadAll(max: Int): Set<ProxyStorageModel> {
        return environment.computeInReadonlyTransaction {
            val openCursor = openStore(it).openCursor(it)
            val result = HashSet<ProxyStorageModel>()
            while (openCursor.next && result.size < max) {
                result.add(deserialize(openCursor.value))
            }
            return@computeInReadonlyTransaction result
        }
    }

    fun iterateAll(consumer: Consumer<ProxyStorageModel>) {
        environment.executeInReadonlyTransaction {
            val openCursor = openStore(it).openCursor(it)
            while (openCursor.next) {
                consumer.accept(deserialize(openCursor.value))
            }
        }
    }

    fun size(): Long {
        var result: Long = 0
        environment.executeInReadonlyTransaction({
            result = openStore(it).count(it)
        })
        return result
    }


    private fun deserialize(entry: ByteIterable) = serializer.deserialize(StringBinding.entryToString(entry))

    private fun getId(proxyStorageModel: ProxyStorageModel): ByteIterableBase {
        return getId(proxyStorageModel.address)
    }

    private fun getId(address: Address): ByteIterableBase = StringBinding.stringToEntry("${address.address}|${address.port}")

}
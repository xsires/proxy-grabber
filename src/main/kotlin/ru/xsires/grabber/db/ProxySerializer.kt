package ru.xsires.grabber.db


import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.stereotype.Component
import ru.xsires.grabber.model.ProxyStorageModel
import java.io.StringWriter

@Component
open class ProxySerializer {
    val mapper = ObjectMapper()

    fun serialize(proxyStorageModel: ProxyStorageModel): String {
        val stringWriter = StringWriter()
        mapper.writeValue(stringWriter, proxyStorageModel)
        return stringWriter.toString()
    }

    fun deserialize(value: String): ProxyStorageModel = mapper.readValue(value, ProxyStorageModel::class.java)
}
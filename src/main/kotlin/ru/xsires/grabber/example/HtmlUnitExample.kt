package ru.xsires.grabber.example

import com.gargoylesoftware.htmlunit.WebClient
import com.gargoylesoftware.htmlunit.html.HtmlElement
import com.gargoylesoftware.htmlunit.html.HtmlPage
import java.nio.file.Files
import java.nio.file.Paths.get


fun main(String: Array<String>) {

    val stream = Files.newInputStream(get("/some/file.txt"))

    stream.buffered().reader().use { reader ->
        println(reader.readText())
    }

    val webClient = WebClient()
    val page = webClient.getPage<HtmlPage>("http://www.freeproxy-list.ru/")
    val allRows = page.getByXPath<HtmlElement>("//div[contains(@class,'table')]/div[contains(@class,'row') and contains(@class,'hover')]")
    val proxies = allRows
            .map { it.childElements.iterator() }
            .map { it.next().asText() + ":" + it.next().asText() }
    println(proxies)


}

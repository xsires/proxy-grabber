package ru.xsires.grabber.example

import java.net.HttpURLConnection
import java.net.InetSocketAddress
import java.net.Proxy
import java.net.URL


fun main(args: Array<String>) {
    val proxy = Proxy(Proxy.Type.HTTP, InetSocketAddress("5.135.195.166", 3128))
    val url = URL("http://www.yahoo.com")
    val uc = url.openConnection(proxy) as HttpURLConnection
    uc.connectTimeout = 10 * 1000
    uc.setRequestProperty("User-Agent",
            "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 (.NET CLR 3.5.30729)")
    uc.connect()
    println(uc.responseCode)

}
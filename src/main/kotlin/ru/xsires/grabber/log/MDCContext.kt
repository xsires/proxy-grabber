package ru.xsires.grabber.log

import org.slf4j.MDC
import org.springframework.stereotype.Component
import java.util.*


@Component
open class MDCContext : AutoCloseable {

    private val NAME = "name"
    private val MARK = "mark"

    fun setLogContext(name: String?): MDCContext {
        MDC.put(NAME, name)
        MDC.put(MARK, UUID.randomUUID().toString().substring(1..13).replace("-", ""))
        return this
    }

    override fun close() {
        MDC.clear()
    }


}


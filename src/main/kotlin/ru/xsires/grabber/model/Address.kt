package ru.xsires.grabber.model

import com.fasterxml.jackson.annotation.JsonProperty

open class Address(@JsonProperty("address") val address: String, @JsonProperty("port") val port: Int) {


    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false

        other as Address

        if (address != other.address) return false
        if (port != other.port) return false

        return true
    }

    override fun hashCode(): Int {
        var result = address.hashCode()
        result = 31 * result + port
        return result
    }

    override fun toString(): String {
        return "Address($address:$port)"
    }

}


package ru.xsires.grabber.model

import com.fasterxml.jackson.annotation.JsonProperty
import com.neovisionaries.i18n.CountryCode
import java.util.*


class ProxyStorageModel(@JsonProperty("address") val address: Address,
                        @JsonProperty("type") val type: ProxyType = ProxyType.UNDEFINED,
                        @JsonProperty("create") val create: Date = Date(),
                        @JsonProperty("country") val country: CountryCode = CountryCode.UNDEFINED,
                        @JsonProperty("lastAvailableMerge") val lastAvailable: Date? = null) {

    fun merge(other: ProxyStorageModel?): ProxyStorageModel {
        if (other == null) {
            return this
        }
        val type = if (type == ProxyType.UNDEFINED) other.type else type
        val create = if (create.before(other.create)) create else other.create
        val lastAvailable = lastAvailableMerge(this.lastAvailable, other.lastAvailable)
        val country = if (country == CountryCode.UNDEFINED) other.country else country
        return ProxyStorageModel(address, type, create, country, lastAvailable)
    }

    private fun lastAvailableMerge(a: Date?, b: Date?): Date? {
        return if (a == null) b else if (b == null) a else if (a.after(b)) a else b
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false

        other as ProxyStorageModel

        if (address != other.address) return false

        return true
    }

    override fun hashCode(): Int {
        return address.hashCode()
    }

    override fun toString(): String {
        return "ProxyStorageModel(address=$address, type=$type, create=$create, country=$country, lastAvailable=$lastAvailable)"
    }
}

open class ProxyStorageModelBuilder {
    var address: Address? = null
    var type: ProxyType = ProxyType.UNDEFINED
    var create: Date = Date()
    var country: CountryCode = CountryCode.UNDEFINED
    var lastAvailable: Date? = null

    fun fromProxyStorageModel(model: ProxyStorageModel): ProxyStorageModelBuilder {
        address = model.address
        type = model.type
        create = model.create
        country = model.country
        lastAvailable = model.lastAvailable
        return this
    }

    fun updateLastAvailable(): ProxyStorageModelBuilder {
        this.lastAvailable = Date()
        return this
    }

    fun withType(type: ProxyType): ProxyStorageModelBuilder {
        this.type = type
        return this
    }

    fun withCountry(country: CountryCode): ProxyStorageModelBuilder {
        this.country = country
        return this
    }

    fun build(): ProxyStorageModel {
        return ProxyStorageModel(Objects.requireNonNull(address)!!, type, create, country, lastAvailable)
    }
}
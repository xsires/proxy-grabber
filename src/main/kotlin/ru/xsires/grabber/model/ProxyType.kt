package ru.xsires.grabber.model

enum class ProxyType {
    HTTP, HTTPS, SOCKS, UNDEFINED
}
package ru.xsires.grabber.monitoring

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import ru.xsires.grabber.db.ProxyDao


@RestController
@RequestMapping("monitoring")
open class MonitoringController(@Autowired val dao: ProxyDao) {

    @GetMapping("/info")
    open fun info(): StatisticInfo {
        return StatisticInfo(dao.statistic())
    }
}
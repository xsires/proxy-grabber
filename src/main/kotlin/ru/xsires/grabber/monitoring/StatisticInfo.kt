package ru.xsires.grabber.monitoring

import com.fasterxml.jackson.annotation.JsonProperty


open class StatisticInfo(@JsonProperty("dbStatistic") val dbStatistic: DBStatistic) {

    override fun toString(): String {
        return "StatisticInfo(dbStatistic=$dbStatistic)"
    }
}


open class DBStatistic(@JsonProperty("count") val count: Int,
                       @JsonProperty("availableCount") val availableCount: Int) {

    override fun toString(): String {
        return "DBStatistic(count=$count, availableCount=$availableCount)"
    }

}

package ru.xsires.grabber.parser

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.xsires.grabber.checker.ProxyChecker
import ru.xsires.grabber.db.ProxyDao
import ru.xsires.grabber.model.ProxyStorageModelBuilder
import java.time.Duration
import java.util.function.Consumer


@Component
open class DailyProxyChecker(@Autowired val dao: ProxyDao,
                             @Autowired val checker: ProxyChecker) : ProxyParser {
    override fun parse() {
        dao.iterateAll(Consumer {
            if (checker.check(it.address)) {
                dao.merge(ProxyStorageModelBuilder()
                        .fromProxyStorageModel(it)
                        .updateLastAvailable()
                        .build())
            }

        })
    }

    override fun name(): String {
        return "DailyProxy"
    }

    override fun period(): Duration {
        return Duration.parse("P1D")
    }

}
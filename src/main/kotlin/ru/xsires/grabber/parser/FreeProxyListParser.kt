package ru.xsires.grabber.parser

import com.neovisionaries.i18n.CountryCode
import org.openqa.selenium.By
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.xsires.grabber.db.ProxyDao
import ru.xsires.grabber.model.Address
import ru.xsires.grabber.model.ProxyStorageModel
import ru.xsires.grabber.phantomjs.WebDriverPool
import java.time.Duration
import java.util.*
import java.util.regex.Pattern


@Component
open class FreeProxyListParser(@Autowired val dao: ProxyDao,
                               @Autowired val webDriverPool: WebDriverPool) : ProxyParser {

    companion object {
        val log = LoggerFactory.getLogger(FreeProxyListParser::class.java)
    }

    override fun name(): String {
        return "FreeProxyList"
    }

    override fun period(): Duration {
        return Duration.parse("PT1H")
    }

    val country = Pattern.compile("\\((\\w{2})\\)")!!

    override fun parse() {

        webDriverPool.get().use {
            it.webDriver.get("http://www.freeproxy-list.ru/")
            val rows = it.webDriver.findElement(By.className("container")).findElements(By.className("row"))
            for (row in rows) {
                val columns = row.findElements(By.className("td"))
                if (columns.size == 7 && isIp(columns[0].text) && columns[1].text.matches(Regex("\\d+"))) {
                    val countryStr = columns[3].text
                    val matcher = country.matcher(countryStr)
                    val countryCode: CountryCode
                    if (matcher.find()) {
                        val country = matcher.group(1)
                        countryCode = Optional.ofNullable(CountryCode.getByCode(country, false)).orElse(CountryCode.UNDEFINED)
                    } else {
                        countryCode = CountryCode.UNDEFINED
                    }
                    dao.merge(ProxyStorageModel(address = Address(columns[0].text, columns[1].text.toInt()),
                            country = countryCode))
                }
            }
        }
    }
}




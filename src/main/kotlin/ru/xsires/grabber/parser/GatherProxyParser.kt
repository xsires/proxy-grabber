package ru.xsires.grabber.parser

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.xsires.grabber.db.ProxyDao
import ru.xsires.grabber.model.Address
import ru.xsires.grabber.model.ProxyStorageModel
import ru.xsires.grabber.phantomjs.WebDriverPool
import java.time.Duration
import java.util.*


@Component
open class GatherProxyParser(@Autowired val dao: ProxyDao,
                             @Autowired val webDriverPool: WebDriverPool) : ProxyParser {


    companion object {
        val log = LoggerFactory.getLogger(GatherProxyParser::class.java)
    }

    override fun parse() {
        webDriverPool.get().use {
            parseList(it.webDriver, "http://www.gatherproxy.com/proxylist/anonymity/?t=Transparent")
            parseList(it.webDriver, "http://www.gatherproxy.com/proxylist/anonymity/?t=Elite")
            parseList(it.webDriver, "http://www.gatherproxy.com/proxylist/anonymity/?t=Anonymous")
            parseSocksList(it.webDriver)
        }
    }

    private fun pressShowFullList(webDriver: WebDriver): Boolean {
        val inputElements = webDriver.findElements(By.tagName("input"))
        for (inputElement in inputElements) {
            if (inputElement.getAttribute("value") == "Show Full List") {
                log.info("found Show Full List button")
                inputElement.click()
                return true
            }
        }
        log.warn("not found Show Full List button")
        return false
    }

    private fun pagenavi(webDriver: WebDriver): List<WebElement> {
        val pegNavi = webDriver.findElement(By.className("pagenavi"))
        if (pegNavi != null) {
            return pegNavi.findElements(By.tagName("a"))
        }
        return Collections.emptyList()
    }

    private fun parseList(webDriver: WebDriver, url: String) {
        log.info("Parse url={}", url)
        webDriver.get(url)
        if (pressShowFullList(webDriver)) {
            parseAllTr(webDriver)
            val size = pagenavi(webDriver).size
            for (pageInd in 0..size) {
                val pagenavi = pagenavi(webDriver)
                if (pagenavi.size > pageInd) {
                    pagenavi[pageInd].click()
                    parseAllTr(webDriver)
                } else {
                    break
                }
            }
        }
    }


    private fun parseSocksList(webDriver: WebDriver) {
        webDriver.get("http://www.gatherproxy.com/sockslist")
        parseAllTr(webDriver)
    }

    private fun parseAllTr(webDriver: WebDriver) {
        webDriver.findElements(By.tagName("tr")).forEach { tr ->
            parserTr(tr)
        }
    }

    private fun parserTr(tr: WebElement) {
        val findElements = tr.findElements(By.tagName("td"))
        if (findElements.size > 3
                && isIp(findElements[1].text)
                && findElements[2].text.matches(Regex("\\d+"))) {
            dao.merge(ProxyStorageModel(address = Address(findElements[1].text,findElements[2].text.toInt())))
        }
    }

    override fun name(): String {
        return "Gather"
    }

    override fun period(): Duration {
        return Duration.parse("PT4H")
    }

}
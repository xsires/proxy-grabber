package ru.xsires.grabber.parser

import com.neovisionaries.i18n.CountryCode
import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.xsires.grabber.db.ProxyDao
import ru.xsires.grabber.model.Address
import ru.xsires.grabber.model.ProxyStorageModel
import ru.xsires.grabber.model.ProxyType
import ru.xsires.grabber.phantomjs.WebDriverPool
import java.time.Duration
import java.util.*
import java.util.regex.Pattern


@Component
open class HideMyAssParser(@Autowired val dao: ProxyDao,
                           @Autowired val webDriverPool: WebDriverPool) : ProxyParser {

    companion object {
        val log = LoggerFactory.getLogger(HideMyAssParser::class.java)
    }

    private val spanCountryCode = Pattern.compile("flag-icon-([a-zA-Z]+)")

    override fun parse() {
        webDriverPool.get().use {
            it.webDriver.get("http://hideme.ru/proxy-list/?type=s45")

            var counter = 0
            do {
                log.info("Parse page " + counter)
                val tbodyList = it.webDriver.findElements(By.tagName("tbody"))
                if (tbodyList.size > 0) {
                    if (tbodyList.size != 1) {
                        log.warn("tbodyList wrong size " + tbodyList.size)
                    }
                    val tbody = tbodyList.get(0)
                    val trowList = tbody.findElements(By.tagName("tr"))
                    for (trow in trowList) {
                        val td = trow.findElements(By.tagName("td"))
                        if (td.size == 7) {
                            val ipWebElementText = td[0].text
                            if (!isIp(ipWebElementText)) {
                                log.warn("wrong ip string " + ipWebElementText + " " + trow.text)
                                break
                            }
                            val port = parsePort(td[1].text)
                            if (port == null) {
                                log.warn("wrong port string " + ipWebElementText + " " + trow.text)
                                break
                            }
                            val countryCode = getCountryCode(td[2])

                            val type = getType(td[4])

                            dao.merge(ProxyStorageModel(Address(ipWebElementText, port), type, Date(), countryCode, null))

                        } else {
                            log.warn("trow wrong size " + trow.getText())
                        }
                    }

                } else {
                    log.warn("tbodyList is empty")
                    break
                }

                val arrowRightDiv = it.webDriver.findElement(By.className("arrow__right"))
                var nextPage: WebElement? = null
                if (arrowRightDiv != null) {
                    nextPage = arrowRightDiv.findElement(By.tagName("a"))
                }
                if (nextPage != null) {
                    nextPage.click()
                } else {
                    break
                }
                counter++
            } while (counter < 100)

            log.info("page parsed " + counter)
        }

    }

    override fun name(): String {
        return "Hidemyass"
    }

    override fun period(): Duration {
        return Duration.parse("PT3H")
    }

    internal fun getCountryCode(countryCodeTd: WebElement): CountryCode {
        val span = countryCodeTd.findElement(By.tagName("span"))
        if (span == null) {
            log.warn("Not found span in country td ${countryCodeTd.text}")
            return CountryCode.UNDEFINED
        }
        val aClass = span.getAttribute("class")
        if (aClass == null) {
            log.warn("Not found class in span in td ${countryCodeTd.text}")
            return CountryCode.UNDEFINED
        }
        val matcher = spanCountryCode.matcher(aClass)
        if (matcher.find()) {
            val countryStr = matcher.group(1)

            val result = CountryCode.getByCode(countryStr, false)
            if (result == null) {
                log.warn("Not found by countryStr $countryStr ${countryCodeTd.text}")
                return CountryCode.UNDEFINED
            } else {
                return result
            }

        }
        log.warn("Not found flag-icon in class in span in td ${countryCodeTd.text}")
        return CountryCode.UNDEFINED


    }

    internal fun getType(typeElement: WebElement): ProxyType {

        val text = typeElement.text
        if (text.isEmpty()) {
            log.warn("typeElement typeText is empty ")
            return ProxyType.UNDEFINED
        }

        if (text.contains("HTTPS")) {
            return ProxyType.HTTPS
        } else if (text.contains("SOCKS")) {
            return ProxyType.SOCKS
        } else if (text.contains("HTTP")) {
            return ProxyType.HTTP
        } else {
            log.warn("wrong proxy type {}", text)
            return ProxyType.UNDEFINED
        }
    }
}
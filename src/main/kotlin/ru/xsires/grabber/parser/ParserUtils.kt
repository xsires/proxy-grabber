package ru.xsires.grabber.parser

import java.util.regex.Pattern

fun isIp(ip: String): Boolean {
    val IPv4 = Pattern.compile("\\A(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)(\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)){3}\\z")
    return IPv4.matcher(ip).matches()
}

fun parsePort(port: String?): Int? {
    if (port.isNullOrEmpty()) {
        return null
    }
    try {
        return Integer.parseInt(port)
    } catch (e: Exception) {
        return null
    }
}
package ru.xsires.grabber.parser

import java.time.Duration


interface ProxyParser {

    fun parse()

    fun name(): String

    fun period(): Duration
}
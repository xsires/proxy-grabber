package ru.xsires.grabber.parser

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.xsires.grabber.log.MDCContext
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.ThreadLocalRandom
import java.util.concurrent.TimeUnit
import javax.annotation.PostConstruct


@Component
open class ProxyParserStarter(@Autowired val proxyParsers: List<ProxyParser>,
                              @Autowired val scheduledService: ScheduledExecutorService,
                              @Autowired val MDCContext: MDCContext) {

    companion object {
        val log = LoggerFactory.getLogger(ProxyParserStarter::class.java)
    }

    @PostConstruct
    fun postConstructor() {
        proxyParsers.forEach { parser ->
            val period = parser.period().toMillis()
            val initialDelay = period / 2 + (ThreadLocalRandom.current().nextLong(Math.round(period.toDouble() / 2)))
            log.info("Start parser: name={}, period={}", parser.name(), parser.period())
            scheduledService.scheduleAtFixedRate({
                MDCContext.setLogContext(parser.name()).use {
                    log.info("Start parsing {}", parser.name())
                    parser.parse()

                }
            }, initialDelay, period, TimeUnit.MILLISECONDS)
        }

    }

}


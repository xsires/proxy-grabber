package ru.xsires.grabber.phantomjs

import org.openqa.selenium.WebDriver
import org.openqa.selenium.phantomjs.PhantomJSDriver
import org.openqa.selenium.phantomjs.PhantomJSDriverService
import org.openqa.selenium.remote.DesiredCapabilities
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.xsires.grabber.config.ProxyParserConfig


@Component
open class PhantomJsWebDriverFactory(
        @Autowired val config: ProxyParserConfig) {

    open fun createDriver(): WebDriver {
        val desiredCapabilities = DesiredCapabilities()
        desiredCapabilities.isJavascriptEnabled = true
        desiredCapabilities.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
                config.phantomJSLocation)
        // Mozilla/5.0 (X11; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0
        return PhantomJSDriver(desiredCapabilities)
    }

}

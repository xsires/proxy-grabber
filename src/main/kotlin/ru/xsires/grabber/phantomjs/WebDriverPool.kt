package ru.xsires.grabber.phantomjs

import org.openqa.selenium.WebDriver
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.xsires.grabber.config.ProxyParserConfig
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantLock


@Component
open class WebDriverPool(@Autowired val config: ProxyParserConfig,
                         @Autowired val factory: PhantomJsWebDriverFactory) {
    var capacity: Int = 0
    val lock: Lock = ReentrantLock(true)
    val queue = LinkedBlockingQueue<WebDriver>()
    val size = config.phantomPoolSize

    fun get(): WebDriverCloseable {
        lock.lock()
        try {
            if (queue.size == 0 && capacity != size) {
                capacity++
                return WebDriverCloseable(factory.createDriver(), this)
            } else {
                return WebDriverCloseable(queue.take(), this)
            }
        } finally {
            lock.unlock()
        }
    }
}


open class WebDriverCloseable(var webDriver: WebDriver, val poll: WebDriverPool) : AutoCloseable {

    override fun close() {
        poll.queue.add(webDriver)
    }

}
package ru.xsires.grabber;

import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {Application.class}, webEnvironment = RANDOM_PORT)
public abstract class AbstractTest {

    static {
        final String CONFIG_OVERRIDE = "config.override";
        final String CONFIG_DIRECTORY = "config.directory";

        //--spring.config.location=file:/usr/local/proxy-grabber/conf/proxy-grabber.properties
        System.setProperty(CONFIG_OVERRIDE, "config-override.properties");
        System.setProperty("spring.config.location", System.getProperty("user.dir") + "/install/unix/conf/proxy-grabber.properties");
        System.setProperty(CONFIG_DIRECTORY, System.getProperty("user.dir") + "/install/unix/conf/");
    }
}

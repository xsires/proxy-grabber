package ru.xsires.grabber;

import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.web.client.TestRestTemplate;

import static org.junit.Assert.assertEquals;


public class ApplicationTests extends AbstractTest {

    private final TestRestTemplate restTemplate = new TestRestTemplate();
    private static final Logger log = LoggerFactory.getLogger(ApplicationTests.class);

    @Value("${local.server.port}")
    private int port;

    @Test
    @Ignore
    public void testHelloWorld() {
        log.info("begin");
        final String response = restTemplate.getForObject("http://localhost:" + port, String.class);
        assertEquals("Hello world!", response);
    }
}

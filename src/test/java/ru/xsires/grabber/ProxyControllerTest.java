package ru.xsires.grabber;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.web.client.TestRestTemplate;

/**
 * Created by xsires on 1/9/17.
 */
public class ProxyControllerTest extends AbstractTest {

    private final TestRestTemplate restTemplate = new TestRestTemplate();

    @Value("${local.server.port}")
    private int port;

    @Test
    @Ignore
    public void go(){
        final String response = restTemplate.getForObject("http://localhost:" + port + "/proxy", String.class);
        System.out.println(response);
    }

}

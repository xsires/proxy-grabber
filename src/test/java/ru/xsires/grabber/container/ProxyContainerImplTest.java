package ru.xsires.grabber.container;

import com.neovisionaries.i18n.CountryCode;
import org.junit.Test;
import ru.xsires.grabber.proxy.Proxy;
import ru.xsires.grabber.proxy.ProxyValidationInfo;

import java.net.InetSocketAddress;
import java.util.List;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;

public class ProxyContainerImplTest {

    @Test
    public void removeTest() {
        ProxyContainerImpl proxyContainer = new ProxyContainerImpl(5);

        proxyContainer.put(new Proxy(new InetSocketAddress("localhost", 80), CountryCode.RU, Proxy.Type.HTTP));
        List<ProxyValidationInfo> validationInformationProxies = proxyContainer.get(null, null);
        ProxyValidationInfo proxy = validationInformationProxies.get(0);

        IntStream.range(0, 5).forEach((i) -> proxy.incrementFailCheckAttemptCount());
        proxyContainer.run();

        assertEquals(proxyContainer.get(null, null).size(), 1);
        proxy.incrementFailCheckAttemptCount();

        proxyContainer.run();

        assertEquals(proxyContainer.get(null, null).size(), 0);
    }

}
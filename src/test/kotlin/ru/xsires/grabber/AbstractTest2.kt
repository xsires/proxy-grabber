package ru.xsires.grabber

import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner


@RunWith(SpringJUnit4ClassRunner::class)
@SpringBootTest(classes = arrayOf(ApplicationKt::class), webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
abstract class AbstractTestKt {
    companion object {
        init {
            val CONFIG_DIRECTORY = "config.directory"
            //val CONFIG_DIRECTORY_OVERRIDE = "config.directory.override"
            //--spring.config.location=file:/usr/local/proxy-grabber/conf/proxy-grabber.properties
            //System.setProperty(CONFIG_DIRECTORY_OVERRIDE, "config-override.properties")
            System.setProperty("spring.config.location", System.getProperty("user.dir") + "/install/unix/conf/proxy-grabber.properties")
            System.setProperty(CONFIG_DIRECTORY, System.getProperty("user.dir") + "/install/unix/conf/")
        }
    }
}

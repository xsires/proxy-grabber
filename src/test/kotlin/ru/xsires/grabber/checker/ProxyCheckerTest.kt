package ru.xsires.grabber.checker

import org.junit.Assert
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.embedded.LocalServerPort
import ru.xsires.grabber.AbstractTestKt
import ru.xsires.grabber.model.Address


open class ProxyCheckerTest : AbstractTestKt() {

    @Autowired
    lateinit var checker: ProxyChecker

    @LocalServerPort
    var port: Int = 0

    @Test
    open fun check() {
        Assert.assertTrue(checker.check(Address("localhost", port)))
    }


}

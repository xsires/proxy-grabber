package ru.xsires.grabber.db

import com.neovisionaries.i18n.CountryCode
import org.junit.Assert
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.runners.MethodSorters
import org.springframework.beans.factory.annotation.Autowired
import org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals
import ru.xsires.grabber.AbstractTestKt
import ru.xsires.grabber.model.Address
import ru.xsires.grabber.model.ProxyStorageModel
import ru.xsires.grabber.model.ProxyType
import java.time.LocalDate
import java.util.*
import java.util.concurrent.ThreadLocalRandom
import java.util.function.Consumer
import kotlin.collections.HashSet

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class ProxyDaoTest : AbstractTestKt() {

    @Autowired lateinit var proxyDao: ProxyDao

    val address = "192.168.0.1"
    val port = 8081
    val proxyStorageModel = ProxyStorageModel(Address(address, port), ProxyType.HTTPS,
            Date(LocalDate.parse("2014-02-14").toEpochDay()),
            CountryCode.UNDEFINED,
            Date(LocalDate.parse("2015-02-14").toEpochDay()))

    @Test
    fun db1CleanLoad() {
        proxyDao.clean()
        Assert.assertEquals(0, proxyDao.size())
    }

    @Test
    fun db2SaveLoad() {
        proxyDao.save(proxyStorageModel)
        assertReflectionEquals(proxyStorageModel, proxyDao.loadByAddress(Address(address, port)))
    }

    @Test
    fun db3LoadAll() {
        val all = proxyDao.loadAll(100)
        Assert.assertEquals(1, all.size)
        assertReflectionEquals(proxyStorageModel, all.iterator().next())
    }

    @Test
    fun db4IterateAll() {
        proxyDao.iterateAll(Consumer { Assert.assertEquals(it, proxyStorageModel) })
    }

    @Test
    fun db5SaveBulk() {
        val proxyStorageModels = HashSet<ProxyStorageModel>()

        (0..254).mapTo(proxyStorageModels) { createProxyStorageModel("192.168.0.$it", 8081) }

        proxyDao.save(proxyStorageModels)
        Assert.assertEquals(255, proxyDao.size())

    }

    @Test
    fun db6Merge() {
        val address1 = Address("127.0.0.1", 555)
        val proxyStorageModel1 = ProxyStorageModel(address1, ProxyType.UNDEFINED, Date(LocalDate.parse("2017-05-05").toEpochDay()),
                CountryCode.UNDEFINED,
                null)
        proxyDao.save(proxyStorageModel1)
        val proxyStorageModel2 = ProxyStorageModel(address1, ProxyType.HTTPS, Date(LocalDate.parse("2016-05-05").toEpochDay()),
                CountryCode.RU, Date(LocalDate.parse("2017-05-05").toEpochDay()))
        proxyDao.merge(proxyStorageModel2)
        assertReflectionEquals(proxyStorageModel2, proxyDao.loadByAddress(address1))
    }

}

fun createProxyStorageModel(address: String, port: Int): ProxyStorageModel {
    return ProxyStorageModel(Address(address, port),
            ProxyType.values()[ThreadLocalRandom.current().nextInt(ProxyType.values().size)], Date(), CountryCode.UNDEFINED, Date())
}

package ru.xsires.grabber.db

import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import ru.xsires.grabber.AbstractTestKt
import ru.xsires.grabber.config.ProxyStorageConfig
import ru.xsires.grabber.model.ProxyStorageModel


class ProxyDaoTestLoad : AbstractTestKt() {

    @Autowired lateinit var serializer: ProxySerializer
    @Autowired lateinit var proxyStorageConfig: ProxyStorageConfig
    @Autowired lateinit var proxyDao: ProxyDao

    @Before
    fun beforeMethod() {
        val proxyStorageConfig = ProxyStorageConfig()
        proxyStorageConfig.location = this.proxyStorageConfig.location + "-test"
        proxyDao = ProxyDao(proxyStorageConfig, serializer)
        proxyDao.init()
        proxyDao.clean()
    }

    /**
     * -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath="/root/git/proxy-grabber-kotlin/" -Xmx150m
     */
    @Test
    @Ignore
    fun create10xxInstances() {
        val createProxyStorageModels = HashSet<ProxyStorageModel>()
        for (i in 0..10000000) {
            createProxyStorageModels.add(createProxyStorageModel(i.toString(), 50))
            if (createProxyStorageModels.size == 10000) {
                proxyDao.save(createProxyStorageModels)
                createProxyStorageModels.clear()
            }
        }
    }

}
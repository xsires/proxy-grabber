package ru.xsires.grabber.db

import com.neovisionaries.i18n.CountryCode
import org.junit.Assert.assertEquals
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals
import ru.xsires.grabber.AbstractTestKt
import ru.xsires.grabber.model.Address
import ru.xsires.grabber.model.ProxyStorageModel
import ru.xsires.grabber.model.ProxyType
import java.io.BufferedReader
import java.io.InputStreamReader
import java.time.LocalDate
import java.util.*
import java.util.stream.Collectors


open class ProxySerializerTest : AbstractTestKt() {

    @Autowired
    lateinit var proxySerializer: ProxySerializer

    fun readSample(name: String): String {
        return BufferedReader(InputStreamReader(this.javaClass.getResourceAsStream(name)))
                .lines().collect(Collectors.joining("\n")).replace("(\\s|\\n|\\r)".toRegex(), "")
    }

    @Test
    fun modelWithoutLastAvailable() {
        serializeCheck(ProxyStorageModel(
                Address("192.168.0.1", 8081),
                ProxyType.HTTPS,
                Date(LocalDate.parse("2014-02-14").toEpochDay()),
                CountryCode.UNDEFINED,
                null),
                readSample("modelWithoutLastAvailable.json"))
    }

    @Test
    fun modelWithLastAvailable() {
        serializeCheck(ProxyStorageModel(Address("192.168.0.1", 8081), ProxyType.HTTPS,
                Date(LocalDate.parse("2014-02-14").toEpochDay()),
                CountryCode.AC,
                Date(LocalDate.parse("2015-02-14").toEpochDay())),
                readSample("modelWithLastAvailable.json"))
    }

    fun serializeCheck(proxyStorageModel: ProxyStorageModel, json: String) {
        assertEquals(json, proxySerializer.serialize(proxyStorageModel))
        assertReflectionEquals(proxyStorageModel, proxySerializer.deserialize(json))
    }
}


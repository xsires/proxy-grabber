package ru.xsires.grabber.model

import com.neovisionaries.i18n.CountryCode
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import org.unitils.reflectionassert.ReflectionAssert
import java.time.LocalDate
import java.util.*

@RunWith(Parameterized::class)
class ProxyStorageModelTest(val left: ProxyStorageModel,
                            val right: ProxyStorageModel,
                            val mergeResult: ProxyStorageModel) {

    companion object {
        @JvmStatic
        @Parameterized.Parameters
        fun data(): Collection<Array<ProxyStorageModel>> = listOf(
                arrayOf(
                        ProxyStorageModel(Address("192.168.0.1", 8081), ProxyType.HTTPS,
                                Date(LocalDate.parse("2014-02-14").toEpochDay()),
                                CountryCode.AC,
                                Date(LocalDate.parse("2015-02-14").toEpochDay())),

                        ProxyStorageModel(Address("192.168.0.1", 8081), ProxyType.UNDEFINED,
                                Date(LocalDate.parse("2014-02-14").toEpochDay()),
                                CountryCode.AC,
                                Date(LocalDate.parse("2015-02-14").toEpochDay())),

                        ProxyStorageModel(Address("192.168.0.1", 8081), ProxyType.HTTPS,
                                Date(LocalDate.parse("2014-02-14").toEpochDay()),
                                CountryCode.AC,
                                Date(LocalDate.parse("2015-02-14").toEpochDay()))),

                arrayOf(
                        ProxyStorageModel(Address("192.168.0.1", 8081), ProxyType.HTTPS,
                                Date(LocalDate.parse("2014-02-14").toEpochDay()),
                                CountryCode.AC,
                                Date(LocalDate.parse("2015-02-14").toEpochDay())),

                        ProxyStorageModel(Address("192.168.0.1", 8081), ProxyType.HTTPS,
                                Date(LocalDate.parse("2013-02-14").toEpochDay()),
                                CountryCode.AC,
                                Date(LocalDate.parse("2015-02-14").toEpochDay())),

                        ProxyStorageModel(Address("192.168.0.1", 8081), ProxyType.HTTPS,
                                Date(LocalDate.parse("2013-02-14").toEpochDay()),
                                CountryCode.AC,
                                Date(LocalDate.parse("2015-02-14").toEpochDay()))),

                arrayOf(
                        ProxyStorageModel(Address("192.168.0.1", 8081), ProxyType.HTTPS,
                                Date(LocalDate.parse("2014-02-14").toEpochDay()),
                                CountryCode.AC,
                                Date(LocalDate.parse("2015-02-14").toEpochDay())),

                        ProxyStorageModel(Address("192.168.0.1", 8081), ProxyType.UNDEFINED,
                                Date(LocalDate.parse("2014-02-14").toEpochDay()),
                                CountryCode.AC,
                                Date(LocalDate.parse("2016-02-14").toEpochDay())),

                        ProxyStorageModel(Address("192.168.0.1", 8081), ProxyType.HTTPS,
                                Date(LocalDate.parse("2014-02-14").toEpochDay()),
                                CountryCode.AC,
                                Date(LocalDate.parse("2016-02-14").toEpochDay()))),

                arrayOf(
                        ProxyStorageModel(Address("192.168.0.1", 8081), ProxyType.HTTPS,
                                Date(LocalDate.parse("2014-02-14").toEpochDay()),
                                CountryCode.UNDEFINED,
                                Date(LocalDate.parse("2015-02-14").toEpochDay())),

                        ProxyStorageModel(Address("192.168.0.1", 8081), ProxyType.HTTPS,
                                Date(LocalDate.parse("2014-02-14").toEpochDay()),
                                CountryCode.AF,
                                Date(LocalDate.parse("2015-02-14").toEpochDay())),

                        ProxyStorageModel(Address("192.168.0.1", 8081), ProxyType.HTTPS,
                                Date(LocalDate.parse("2014-02-14").toEpochDay()),
                                CountryCode.AF,
                                Date(LocalDate.parse("2015-02-14").toEpochDay())))
        )
    }


    @Test
    fun merge() {
        ReflectionAssert.assertReflectionEquals(mergeResult, left.merge(right))
    }
}
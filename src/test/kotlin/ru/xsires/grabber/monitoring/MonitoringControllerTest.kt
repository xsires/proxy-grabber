package ru.xsires.grabber.monitoring

import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.Assert
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.embedded.LocalServerPort
import org.springframework.boot.test.web.client.TestRestTemplate
import ru.xsires.grabber.AbstractTestKt


class MonitoringControllerTest : AbstractTestKt() {

    @Autowired
    lateinit var restTemplate: TestRestTemplate

    @LocalServerPort
    var port: Int = 0

    @Test
    fun pingIsOk() {
        val result = restTemplate.getForObject("http://localhost:$port/monitoring/info", String::class.java)
        val readValue = ObjectMapper().readValue<StatisticInfo>(result, StatisticInfo::class.java)
        Assert.assertNotNull(readValue)
    }

}
package ru.xsires.grabber.parser

import org.junit.Ignore
import org.junit.Test
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import ru.xsires.grabber.AbstractTestKt


open class FreeProxyListParserTest : AbstractTestKt() {

    @Autowired lateinit var freeProxyListParser: FreeProxyListParser

    companion object {
        val log = LoggerFactory.getLogger(FreeProxyListParserTest::class.java)
    }


    @Test
    @Ignore
    open fun parseTest() {
        log.info("Start parse {}", freeProxyListParser.name())
        freeProxyListParser.parse()
    }
}
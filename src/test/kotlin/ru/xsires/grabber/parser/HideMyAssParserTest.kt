package ru.xsires.grabber.parser

import org.junit.Ignore
import org.junit.Test
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import ru.xsires.grabber.AbstractTestKt


open class HideMyAssParserTest : AbstractTestKt() {

    @Autowired lateinit var hideMyAssParser: HideMyAssParser

    companion object {
        val log = LoggerFactory.getLogger(FreeProxyListParserTest::class.java)
    }


    @Test
    @Ignore
    open fun parseTest() {
        log.info("Start parse {}", hideMyAssParser.name())
        hideMyAssParser.parse()
    }
}